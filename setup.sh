#! /bin/bash

# Clear everything, I have nothing important
docker rm -f $(docker ps -aq) >/dev/null
docker rmi -f $(docker images -q) >/dev/null


image_id=$(docker build --force-rm --quiet --tag 'ubuntu-strace' .)
image_id=${image_id:7}
echo ${image_id}
docker run --detach --tty --interactive --cap-add=SYS_PTRACE --security-opt seccomp=unconfined -v $(pwd):/root --name "ubuntu-strace" ${image_id}

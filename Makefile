
NAME			= strace

CC				= gcc

VPATH			= ./srcs/

SRCS			= get_bin.c strace.c trace.c array.c

INCS			= ./incs/

OBJS			= $(SRCS:.c=.o)

override CFLAGS	+= -Wall -Werror -Wextra -Ofast -std=gnu11


all: $(NAME)

$(NAME): $(OBJS)
	$(CC) $(OBJS) -o $@

%.o: %.c Makefile
	$(CC) $(CFLAGS) -I $(INCS) -o $@ -c $<

clean:
	rm -f $(OBJS)

fclean:
	rm -f $(OBJS)
	rm -f $(NAME)

re: fclean all

setup:
	bash setup.sh

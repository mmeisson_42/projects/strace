
FROM ubuntu

RUN apt update && apt upgrade -y && apt install -y \
		gcc \
		man \
		vim \
		make \
		strace \
		python

WORKDIR /root

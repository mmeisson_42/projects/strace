#!/usr/bin/env python

import re

SYS_SRC = "/usr/include/x86_64-linux-gnu/asm/unistd_64.h"
SYS_MAN = "/usr/share/man/man2/"
SYS_ARRAY = "srcs/array.c"

SYSCALL_DEF = r"[a-zA-Z_]+ ([a-zA-Z])\(((?:[a-zA-Z\*,_- ])+)\)"

UNKNOWN = "(unknown)"

FILE = """\

struct      syscall
{{
    int		id;
    char    *   name;
	
}}		s_syscall;
"""

def get_syscalls():
    syscalls = {}
    with open(SYS_SRC) as sys_file:
        for line in sys_file:
            if line.startswith("#define __NR_"):
                line = line.replace("#define __NR_", "").split()
            syscall_name = line[0]
            syscall_number = int(line[1])
            syscalls[syscall_name] = {"syscall_number": syscall_number}
    return syscalls


if __name__ == "__main__":
    syscalls = get_syscalls()

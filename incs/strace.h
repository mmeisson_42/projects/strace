
#ifndef STRACE_H
# define STRACE_H

# include <unistd.h>
# include <sys/types.h>

# define REG_NUMBER		27

typedef unsigned char	elf_class;

typedef struct		bin
{
	char const	*	name;
	elf_class		class;
}					s_bin;

/*
 *	Call bin should not return unless an error occurs
 *	It should instead use execve syscall
 */
char		*get_bin(char const * bin);

void		trace(pid_t child);

#endif /* STRACE_H */

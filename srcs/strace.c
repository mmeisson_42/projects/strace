/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strace.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/08 13:51:53 by mmeisson          #+#    #+#             */
/*   Updated: 2017/12/11 10:52:56 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/ptrace.h>

#include "strace.h"

extern char		**environ;

int		main(int argc, char **argv)
{
	pid_t		child;
	char		*bin;

	if (argc < 2) {
		fprintf(stderr, "Usage: %s <bin>\n", argv[0]);
		return EXIT_FAILURE;
	}
	bin = get_bin(argv[1]);

	if (bin == NULL) {
		fprintf(stderr, "Usage: %s <bin>\nBinary %s not found",
				argv[0], argv[1]);
		return EXIT_FAILURE;
	}
	if ((child = fork()) == 0) {
		/* Child, call required process */
		if (bin != NULL) {
			kill(getpid(), SIGSTOP);
			execve(bin, argv + 1, environ);
		}
	}
	else {
		free(bin);
		trace(child);
		/* Father, trace process */
	}
	return EXIT_SUCCESS;
}

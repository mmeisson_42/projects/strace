/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   trace.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/08 13:54:40 by mmeisson          #+#    #+#             */
/*   Updated: 2017/12/11 12:08:38 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/ptrace.h>
#include <sys/reg.h>
#include <sys/wait.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include "strace.h"

extern char const *syscalls[];

	static unsigned long long int
*get_args(unsigned long long int *registers, unsigned int args_number)
{
	unsigned int const		args_index[6] = {
		[0] = RDI,
		[1] = RSI,
		[2] = RDX,
		[3] = RCX,
		[4] = R8,
		[5] = R9,
	};
	unsigned long long int	*args;

	args = calloc(sizeof(* args), args_number);
	if (args == NULL) {
		perror(NULL);
		exit(1);
	}
	for (unsigned int i = 0; i < args_number; i++) {
		args[i] = registers[args_index[i]];
	}
	return args;
}

void	trace(pid_t child)
{
	int							status;
	unsigned long long int		registers[REG_NUMBER] = {0};
	unsigned long long int		*args = NULL;

	if (waitpid(child, &status, WUNTRACED) == -1) {
		perror(NULL);
		return ;
	}
	ptrace(PTRACE_SEIZE, child, NULL, NULL);
	while (1) {
		ptrace(PTRACE_SYSCALL, child, NULL, NULL);
		waitpid(child, &status, 0);
		if (WIFEXITED(status)) {
			break ;
		}
		else if (WIFSTOPPED(status)) {
			ptrace(PTRACE_GETREGS, child, NULL, registers);
			args = get_args(registers, 1);
			ptrace(PTRACE_SYSCALL, child, NULL, NULL);
			waitpid(child, &status, 0);
			ptrace(PTRACE_GETREGS, child, NULL, registers);
			printf("%-40s() = %llu\n", syscalls[registers[ORIG_RAX]], registers[RAX]);
		}
		else if (WIFSIGNALED(status)) {
			printf("--- SIG %d ---\n", WTERMSIG(status));
		}
		if (args != NULL) {
			free(args);
		}
	}
}

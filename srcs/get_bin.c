/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   call_bin.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/07 15:53:22 by mmeisson          #+#    #+#             */
/*   Updated: 2017/12/10 12:04:32 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/ptrace.h>
#include <sys/types.h>

static char		*test_path(char const * bin, char const * path)
{
	size_t	bin_len = strlen(bin);
	size_t	path_len = strlen(path);
	char	*full_bin = NULL;

	full_bin = calloc(sizeof(char), bin_len + path_len + 2);
	if (full_bin == NULL) {
		perror(NULL);
		return NULL;
	}

	strcpy(full_bin, path);
	if (full_bin[path_len - 1] == '/') {
		strcpy(full_bin + path_len, bin);
	}
	else {
		full_bin[path_len] = '/';
		strcpy(full_bin + path_len + 1, bin);
	}
	if (access(full_bin, F_OK | X_OK) == -1) {
		free(full_bin);
		full_bin = NULL;
	}
	return full_bin;
}

char		*get_bin(char const * bin)
{
	char		*full_bin = NULL;

	if (bin[0] != '/' && bin[0] != '.') {
		char	*path = getenv("PATH");
		char	*token;
		char	*path_origin;

		if (path == NULL) {
			return NULL;
		}
		path = strdup(path);
		if (path == NULL) {
			perror(NULL);
			return NULL;
		}
		path_origin = path;
		while ((token = strsep(&path, ":")) != NULL) {
			full_bin = test_path(bin, token);
			if (full_bin != NULL) {
				break ;
			}
		}
		free(path_origin);
	}
	else {
		full_bin = strdup(bin);	/* A bit ugly, but bin is then always freeable */
	}
	return full_bin;
}
